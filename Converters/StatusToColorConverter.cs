﻿using FSWatcher.Models;
using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace FSWatcher.Converters
{
    class StatusToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var status = (DirectoryChangesListModel.Status)value;
            switch (status)
            {
                case DirectoryChangesListModel.Status.NotProcessed:
                    return Brushes.Black;
                case DirectoryChangesListModel.Status.Processing:
                    return Brushes.Blue;
                case DirectoryChangesListModel.Status.Processed:
                    return Brushes.Green;
                default:
                    return Brushes.Red;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}