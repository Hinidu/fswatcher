﻿using System;
using System.Collections;
using System.Globalization;
using System.Windows.Data;

namespace FSWatcher.Converters
{
    class SingletonConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return new[] { value };
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var enumerator = ((IEnumerable)value).GetEnumerator();
            enumerator.MoveNext();
            return enumerator.Current;
        }
    }
}