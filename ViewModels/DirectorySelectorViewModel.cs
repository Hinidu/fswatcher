﻿using FSWatcher.Events;
using FSWatcher.Models;
using ReactiveUI;
using System;
using System.Windows.Forms;

namespace FSWatcher.ViewModels
{
    class DirectorySelectorViewModel : ReactiveObject
    {
        private DirectorySelectorModel model;

        public string Path
        {
            get { return model.Path; }
            private set { this.RaiseAndSetIfChanged(ref model.Path, value); }
        }

        public IObservable<DirectorySelectedEvent> DirectorySelected
        {
            get
            {
                return this.ObservableForProperty(
                    x => x.Path,
                    x => new DirectorySelectedEvent(x, model.IsValid));
            }
        }

        public IReactiveCommand<object> Browse { get; private set; }

        public DirectorySelectorViewModel()
        {
            model = new DirectorySelectorModel();

            Browse = ReactiveCommand.Create();
            Browse.Subscribe(_ =>
            {
                string path = ShowSelectorDialog();
                if (!string.IsNullOrEmpty(path))
                {
                    Path = path;
                }
            });

            MessageBus.Current.RegisterMessageSource(DirectorySelected);
        }

        private string ShowSelectorDialog()
        {
            var dialog = new FolderBrowserDialog();
            var selected = dialog.ShowDialog();
            return selected == DialogResult.OK ? dialog.SelectedPath : null;
        }
    }
}