﻿using FSWatcher.Events;
using FSWatcher.Models;
using Microsoft.Win32;
using ReactiveUI;
using System;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace FSWatcher.ViewModels
{
    public class MainViewModel : ReactiveObject
    {
        public IReactiveCommand<object> StartWatching { get; private set; }

        public IReactiveCommand<object> Pack { get; private set; }

        public IReactiveCommand<object> StopWatching { get; private set; }

        private bool _IsWatching = false;
        public bool IsWatching
        {
            get { return _IsWatching; }
            private set { this.RaiseAndSetIfChanged(ref _IsWatching, value); }
        }

        private bool _IsPacking = false;
        public bool IsPacking
        {
            get { return _IsPacking; }
            private set { this.RaiseAndSetIfChanged(ref _IsPacking, value); }
        }

        public IObservable<bool> IsWatchingChanged { get; private set; }

        private DirectoryChangesModel _DirectoryChanges;
        public DirectoryChangesModel DirectoryChanges
        {
            get { return _DirectoryChanges; }
            private set { this.RaiseAndSetIfChanged(ref _DirectoryChanges, value); }
        }

        private IDisposable WatchingSubscription;

        public MainViewModel()
        {
            string path = null;

            var directorySelected = MessageBus.Current.Listen<DirectorySelectedEvent>();
            directorySelected.Where(e => e.IsValid).Subscribe(e => path = e.Path);

            StartWatching = ReactiveCommand.Create(
                Observable.CombineLatest(
                    this.WhenAny(x => x.IsWatching, x => x.Value),
                    directorySelected.Select(e => e.IsValid),
                    (isWatching, directoryIsValid) => !isWatching && directoryIsValid));
            StartWatching.Subscribe(_ => OnStartWatching(path));

            var canPackOrStop = this.WhenAny(
                x => x.IsWatching,
                x => x.IsPacking,
                (isWatching, isPacking) => isWatching.GetValue() && !isPacking.GetValue());

            Pack = ReactiveCommand.Create(canPackOrStop);
            Pack.Subscribe(_ => OnPack());

            StopWatching = ReactiveCommand.Create(canPackOrStop);
            StopWatching.Subscribe(_ => OnStopWatching());

            IsWatchingChanged =
                Observable.Merge(
                    StartWatching.Select(_ => true),
                    StopWatching.Select(_ => false));

            IsWatchingChanged.Subscribe(isWatching => IsWatching = isWatching);
        }

        private void OnStartWatching(string path)
        {
            DirectoryChanges = new DirectoryChangesModel(path, false);
            WatchingSubscription = DirectoryChanges.StartWatching();
        }

        private void OnStopWatching()
        {
            WatchingSubscription.Dispose();
            DirectoryChanges = null;
        }

        private void OnPack()
        {
            var dialog = new SaveFileDialog { Filter = "Zip Archive (*.zip)|*.zip" };
            var selected = dialog.ShowDialog();
            if (selected == true)
            {
                DirectoryChanges.PauseWatching();
                IsPacking = true;
                Task.Factory.StartNew(() => DirectoryChanges.Pack(dialog.FileName))
                    .ContinueWith(_ =>
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            DirectoryChanges.Clear();
                            DirectoryChanges.ContinueWatching();
                            IsPacking = false;
                        });
                    });
            }
        }
    }
}