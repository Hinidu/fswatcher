﻿
namespace ReactiveUI
{
    /// <summary>
    /// Straightforward ReactiveSortedSet based on the ReactiveList.
    /// </summary>
    /// <remarks>
    /// Invariant is maintained only in Add(), do not use other "adding" methods.
    /// </remarks>
    public class ReactiveSortedSet<T> : ReactiveList<T>
    {
        public override void Add(T item)
        {
            int index = BinarySearch(item);
            if (index < 0)
            {
                Insert(~index, item);
            }
        }

        public override bool Remove(T item)
        {
            int index = BinarySearch(item);
            if (index >= 0)
            {
                RemoveAt(index);
            }
            return index >= 0;
        }
    }
}