﻿using ReactiveUI;
using System;

namespace FSWatcher.Models
{
    public class DirectoryChangesListModel
        : ReactiveObject, IEquatable<DirectoryChangesListModel>
        , IComparable<DirectoryChangesListModel>
    {
        public enum Status
        {
            NotProcessed,
            Processing,
            Processed,
        }

        private string _Name;
        public string Name
        {
            get { return _Name; }
            private set { this.RaiseAndSetIfChanged(ref _Name, value); }
        }

        private Status _CurrentStatus;
        public Status CurrentStatus
        {
            get { return _CurrentStatus; }
            set { this.RaiseAndSetIfChanged(ref _CurrentStatus, value); }
        }

        public DirectoryChangesListModel(string name)
        {
            Name = name;
            CurrentStatus = Status.NotProcessed;
        }

        public bool Equals(DirectoryChangesListModel other)
        {
            return other != null && String.Equals(this.Name, other.Name);
        }

        public int CompareTo(DirectoryChangesListModel other)
        {
            return other == null ? 1 : String.Compare(this.Name, other.Name);
        }
    }
}