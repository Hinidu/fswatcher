﻿using System.IO;

namespace FSWatcher.Models
{
    class DirectorySelectorModel
    {
        public string Path;

        public bool IsValid
        {
            get { return Directory.Exists(Path); }
        }
    }
}