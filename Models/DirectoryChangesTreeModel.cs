﻿using ReactiveUI;
using System;
using System.IO;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows;

namespace FSWatcher.Models
{
    public class DirectoryChangesTreeModel
        : ReactiveObject, IEquatable<DirectoryChangesTreeModel>, IComparable<DirectoryChangesTreeModel>
    {
        private string _Name;
        public string Name
        {
            get { return _Name; }
            private set { this.RaiseAndSetIfChanged(ref _Name, value); }
        }

        private bool _IsChanged = false;
        public bool IsChanged
        {
            get { return _IsChanged; }
            set { this.RaiseAndSetIfChanged(ref _IsChanged, value); }
        }

        public ReactiveList<DirectoryChangesTreeModel> Children { get; private set; }

        public DirectoryChangesTreeModel(string name, bool isChanged = false)
        {
            Name = name;
            IsChanged = isChanged;
            Children = new ReactiveList<DirectoryChangesTreeModel>();
        }

        public void Add(string path)
        {
            int separatorIndex = path.IndexOf(Path.DirectorySeparatorChar);
            if (separatorIndex == -1)
            {
                EnsureChild(path, true);
            }
            else
            {
                var subfolder = EnsureChild(path.Substring(0, separatorIndex), false);
                subfolder.Add(path.Substring(separatorIndex + 1));
            }
        }

        public void Remove(string path)
        {
            int separatorIndex = path.IndexOf(Path.DirectorySeparatorChar);
            string childName = separatorIndex == -1 ? path : path.Substring(0, separatorIndex);
            int childIndex = Children.BinarySearch(new DirectoryChangesTreeModel(childName));
            if (childIndex >= 0)
            {
                if (separatorIndex == -1)
                {
                    Children.RemoveAt(childIndex);
                    this.IsChanged = true;
                }
                else
                {
                    Children[childIndex].Remove(path.Substring(separatorIndex + 1));
                }
            }
            else
            {
                if (separatorIndex == -1)
                {
                    this.IsChanged = true;
                }
                else
                {
                    Children.Insert(~childIndex, new DirectoryChangesTreeModel(childName, false));
                    Children[~childIndex].Remove(path.Substring(separatorIndex + 1));
                }
            }
        }

        private DirectoryChangesTreeModel EnsureChild(string name, bool isChanged)
        {
            var child = new DirectoryChangesTreeModel(name, isChanged);
            int childIndex = Children.BinarySearch(child);
            if (childIndex >= 0)
            {
                child = Children[childIndex];
            }
            else
            {
                // If the BinarySearch didn't find child with this name
                // then it returns binary complement of the index of
                // the first child with the lexicographically bigger name
                // (or Count if there is no "bigger" child).
                Children.Insert(~childIndex, child);
            }
            return child;
        }

        public bool Equals(DirectoryChangesTreeModel other)
        {
            return other != null && String.Equals(this.Name, other.Name);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as DirectoryChangesTreeModel);
        }

        public override int GetHashCode()
        {
            return Name == null ? 42 : Name.GetHashCode();
        }

        public int CompareTo(DirectoryChangesTreeModel other)
        {
            return other == null ? 1 : String.Compare(this.Name, other.Name);
        }
    }
}