﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows;

namespace FSWatcher.Models
{
    public class DirectoryChangesModel : ReactiveObject
    {
        private string RootPath;

        private FileSystemWatcher Watcher;

        private DirectoryChangesTreeModel _Tree;
        public DirectoryChangesTreeModel Tree
        {
            get { return _Tree; }
            private set { this.RaiseAndSetIfChanged(ref _Tree, value); }
        }

        private ReactiveSortedSet<DirectoryChangesListModel> _List;
        public ReactiveSortedSet<DirectoryChangesListModel> List
        {
            get { return _List; }
            private set { this.RaiseAndSetIfChanged(ref _List, value); }
        }

        public DirectoryChangesModel(string path, bool isChanged = false)
        {
            // FileSystemWatcher doesn't work with paths like C:, change it to C:\
            if (path.EndsWith(":"))
            {
                path += Path.DirectorySeparatorChar;
            }

            RootPath = path;
            Tree = new DirectoryChangesTreeModel(path, isChanged);
            List = new ReactiveSortedSet<DirectoryChangesListModel>();
        }

        public void Add(string path)
        {
            Tree.Add(path);
            List.Add(new DirectoryChangesListModel(path));
        }

        public void Remove(string path)
        {
            Tree.Remove(path);
            List.Remove(new DirectoryChangesListModel(path));

            // Add parent folder to changes list
            var directory = Path.GetDirectoryName(path);
            if (!string.IsNullOrEmpty(directory))
            {
                List.Add(new DirectoryChangesListModel(directory));
            }

            // Remove descendants if path points to a directory
            path = path.EnsureEndsWith(Path.DirectorySeparatorChar);
            int index = List.BinarySearch(new DirectoryChangesListModel(path));
            if (index < 0)
            {
                index = ~index;
            }
            while (index < List.Count && List[index].Name.StartsWith(path))
            {
                List.RemoveAt(index);
            }
        }

        public void Clear()
        {
            Tree.IsChanged = false;
            Tree.Children.Clear();
            List.Clear();
        }

        public IDisposable StartWatching()
        {
            Watcher = new FileSystemWatcher(RootPath)
            {
                EnableRaisingEvents = true,
                IncludeSubdirectories = true,
            };

            var dispatcher = Application.Current.Dispatcher;

            var changedOrCreatedSubscription =
                Observable.Merge(
                    Observable.FromEventPattern<FileSystemEventArgs>(Watcher, "Changed"),
                    Observable.FromEventPattern<FileSystemEventArgs>(Watcher, "Created"))
                .ObserveOn(dispatcher)
                .Subscribe(x => Add(x.EventArgs.Name));

            var deletedSubscription =
                Observable.FromEventPattern<FileSystemEventArgs>(Watcher, "Deleted")
                .ObserveOn(dispatcher)
                .Subscribe(x => Remove(x.EventArgs.Name));

            var renamedSubscription =
                Observable.FromEventPattern<RenamedEventArgs>(Watcher, "Renamed")
                .ObserveOn(dispatcher)
                .Subscribe(x =>
                {
                    Remove(x.EventArgs.OldName);
                    Add(x.EventArgs.Name);
                });

            return Disposable.Create(() =>
            {
                changedOrCreatedSubscription.Dispose();
                deletedSubscription.Dispose();
                renamedSubscription.Dispose();
                Watcher.Dispose();
                Watcher = null;
            });
        }

        public void PauseWatching()
        {
            Watcher.EnableRaisingEvents = false;
        }

        public void ContinueWatching()
        {
            Watcher.EnableRaisingEvents = true;
        }

        public void Pack(string zipPath)
        {
            using (var zipStream = new FileStream(zipPath, FileMode.Create))
            using (var zip = new ZipArchive(zipStream, ZipArchiveMode.Create))
            {
                string lastDirectory = null;
                foreach (var file in List)
                {
                    if (!string.IsNullOrEmpty(lastDirectory)
                        && file.Name.StartsWith(lastDirectory))
                    {
                        file.CurrentStatus = DirectoryChangesListModel.Status.Processed;
                        continue;
                    }
                    file.CurrentStatus = DirectoryChangesListModel.Status.Processing;
                    string filePath = Path.Combine(RootPath, file.Name);
                    if (Directory.Exists(filePath))
                    {
                        ZipDirectory(zip, file.Name, filePath);
                        lastDirectory = file.Name.EnsureEndsWith(Path.DirectorySeparatorChar);
                    }
                    else
                    {
                        ZipFile(zip, file.Name, filePath);
                    }
                    Application.Current.Dispatcher.Invoke(() =>
                        file.CurrentStatus = DirectoryChangesListModel.Status.Processed);
                }
            }
        }

        private void ZipDirectory(ZipArchive zip, string directoryName, string directoryPath)
        {
            IEnumerable<string> filePaths = null;
            try
            {
                filePaths = Directory.EnumerateFiles(directoryPath, "*.*", SearchOption.AllDirectories);
            }
            catch (UnauthorizedAccessException)
            {
                return;
            }
            foreach (var filePath in filePaths)
            {
                ZipFile(
                    zip,
                    filePath.Substring(RootPath.Length).TrimStart(Path.DirectorySeparatorChar),
                    filePath);
            }
        }

        private void ZipFile(ZipArchive zip, string fileName, string filePath)
        {
            var zipEntry = zip.CreateEntry(fileName);
            byte[] fileContent = null;
            try
            {
                fileContent = File.ReadAllBytes(filePath);
            }
            catch (IOException)
            {
                return;
            }
            catch (UnauthorizedAccessException)
            {
                return;
            }
            using (var stream = zipEntry.Open())
            {
                stream.Write(fileContent, 0, fileContent.Length);
            }
        }
    }
}