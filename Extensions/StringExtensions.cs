﻿
namespace System
{
    public static class StringExtensions
    {
        public static string EnsureEndsWith(this string This, string desiredSuffix)
        {
            return This.EndsWith(desiredSuffix) ? This : This + desiredSuffix;
        }

        public static string EnsureEndsWith(this string This, char desiredLastChar)
        {
            return EnsureEndsWith(This, desiredLastChar.ToString());
        }
    }
}