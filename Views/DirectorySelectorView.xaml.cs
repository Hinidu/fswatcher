﻿using System.Windows.Controls;
using FSWatcher.ViewModels;

namespace FSWatcher.Views
{
    public partial class DirectorySelectorView : UserControl
    {
        public DirectorySelectorView()
        {
            InitializeComponent();
            DataContext = new DirectorySelectorViewModel();
        }
    }
}