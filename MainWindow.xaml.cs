﻿using System.Windows;
using FSWatcher.ViewModels;
using ReactiveUI;

namespace FSWatcher
{
    public partial class MainWindow : Window, IViewFor<MainViewModel>
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = ViewModel = new MainViewModel();
            this.OneWayBind(
                ViewModel,
                viewModel => viewModel.IsWatching,
                view => view.DirectorySelector.IsEnabled,
                x => !x);
        }

        public MainViewModel ViewModel
        {
            get { return (MainViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        object IViewFor.ViewModel
        {
            get { return ViewModel; }
            set { ViewModel = (MainViewModel)value; }
        }

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(MainViewModel), typeof(MainWindow), new PropertyMetadata(null));
    }
}