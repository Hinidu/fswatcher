﻿
namespace FSWatcher.Events
{
    class DirectorySelectedEvent
    {
        public string Path { get; private set; }

        public bool IsValid { get; private set; }

        public DirectorySelectedEvent(string path, bool isValid)
        {
            Path = path;
            IsValid = isValid;
        }
    }
}